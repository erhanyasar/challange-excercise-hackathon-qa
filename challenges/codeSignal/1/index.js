function solution(s) {
  let stringSet = new Set(),
    palindromesArr = new Array(),
    longestPalindrome = "";

  for (let i = 0; i < s.length; i++) stringSet.add(s.substring(0, i + 1));

  function findPalindrome() {
    stringSet.forEach((word) => {
      if (word.split("").reverse().join("") === word) palindromesArr.push(word);
    });
  }
  findPalindrome();

  longestPalindrome =
    palindromesArr.length !== 0
      ? palindromesArr[palindromesArr.length - 1]
      : "";

  if (longestPalindrome.length > 1 && longestPalindrome.length !== 0)
    return solution(s.replace(longestPalindrome, ""));
  else return s;
}

console.log(solution("codesignal"));
// aaacodedoc
