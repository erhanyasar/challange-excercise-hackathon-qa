function solution(a) {
  let counter = 0;

  for (let i = 0; i < a.length - 1; i++) {
    for (let j = i + 1; j < a.length; j++) {
      if (isDigitAnagram(a[i], a[j])) counter++;
    }
  }

  function isDigitAnagram(a, b) {
    let matchesCount = 0,
      aDigits = a.toString().split(""),
      bDigits = b.toString().split("");

    if (aDigits.length === bDigits.length) {
      for (let i = 0; i < aDigits.length; i++) {
        for (let j = 0; j < bDigits.length; j++) {
          console.log(aDigits[i], bDigits[j]);
          if (aDigits[i] === bDigits[j]) {
            // console.log("i;", aDigits[i], matchesCount, j)
            matchesCount++;
            j += 1;
          } else if (j + 1 === bDigits.length) {
            j += 1;
          }
        }
      }
    } else return false;
    return matchesCount === aDigits.length ? true : false;
  }

  return counter;
}

console.log(solution([25, 35, 872, 228, 53, 278, 872]));
