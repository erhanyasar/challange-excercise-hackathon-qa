## Scenario Step-1:

Your task is to implement a simple container of integer numbers. As a first step, consider implementing the following two operations:

    ADD <value> should add the specified integer value to the container. Returns an empty string.
    EXISTS <value> should check whether the specific integer value exists in the container. Returns "true" if the value exists, "false" otherwise.

The container is supposed to be empty at the beginning of execution.

Given a list of queries, return the list of answers for these queries. To pass to the next level you have to pass all tests at this level.

## Example:

queries = [
["ADD", "1"],
["ADD", "2"],
["ADD", "5"],
["ADD", "2"],
["EXISTS", "2"],
["EXISTS", "5"],
["EXISTS", "1"],
["EXISTS", "4"],
["EXISTS", "3"],
["EXISTS", "0"]
]

the output should be solution(queries) = ["", "", "", "", "true", "true", "true", "false", "false", "false"].

## Explanation:

    Add 1, 2, 5, 2 -> [1, 2, 5, 2]
    Numbers 2, 5, 1 exist in the container
    Numbers 4, 3, 0 don't exist in the container

## Scenario Step-2:

The next step is to support removal from the container:

    REMOVE <value> should remove a single occurrence of the specified value from the container. If the value has multiple occurrences, only one of them should be removed.

Previous level functionality should remain the same. To pass to the next level you have to pass all tests at this level.

Given a list of queries, return the list of answers for these queries.

## Example:

queries = [
["ADD", "1"],
["ADD", "2"],
["ADD", "2"],
["ADD", "3"],
["EXISTS", "1"],
["EXISTS", "2"],
["EXISTS", "3"],
["REMOVE", "2"],
["REMOVE", "1"],
["EXISTS", "2"],
["EXISTS", "1"]
]

the output should be solution(queries) = ["", "", "", "", "true", "true", "true", "true", "true", "true", "false"].

## Explanation:

    Add 1, 2, 2, 3 -> [1, 2, 2, 3]
    Numbers 1, 2, 3 exist in the container
    Remove 2, 1 -> [2, 3]
    Number 2 exists in the container, number 1 doesn't exist

## Scenario Step-3:

The next step is to support the operation for finding the nearest integer in the container greater than the provided one:

    GET_NEXT <value> should return the minimal integer in the container that is strictly greater than the provided value. In case there is no such integer in the container, return empty string.

Previous levels functionality should remain the same. To pass to the next level you have to pass all tests at this level.

Given a list of queries, return the list of answers for these queries.

## Example:

queries = [
["ADD", "1"],
["ADD", "2"],
["ADD", "2"],
["ADD", "4"],
["GET_NEXT", "1"],
["GET_NEXT", "2"],
["GET_NEXT", "3"],
["GET_NEXT", "4"],
["REMOVE", "2"],
["GET_NEXT", "1"],
["GET_NEXT", "2"],
["GET_NEXT", "3"],
["GET_NEXT", "4"]
]

the output should be solution(queries) = ["", "", "", "", "2", "4", "4", "", "true", "2", "4", "4", ""].

## Explanation:

    Add 1, 2, 2, 4 -> [1, 2, 2, 4]
    Get Next 1 -> "2"
    Get Next 2 -> "4"
    Get Next 3 -> "4"
    Get Next 4 -> ""
    Remove 2 -> [1, 2, 4]
    Get Next 1 -> "2"
    Get Next 2 -> "4"
    Get Next 3 -> "4"
    Get Next 4 -> ""
