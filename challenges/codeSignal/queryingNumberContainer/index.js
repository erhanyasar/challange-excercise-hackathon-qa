function solution(queries) {
  let container = [],
    result = [];

  result = queries.map((query) => {
    if (query[0] === "ADD") {
      container.push(query[1]);
      return "";
    } else if (query[0] === "EXISTS") {
      if (container.includes(query[1])) return "true";
      else return "false";
    } else if (query[0] === "REMOVE") {
      if (container.includes(query[1])) {
        let indexToRemove = container.indexOf(query[1]);
        container.splice(indexToRemove, 1);
        return "true";
      } else return "false";
    } else if (query[0] === "GET_NEXT") {
      nextValues = container.filter((value) => Number(value) > query[1]);
      nextValues?.sort((a, b) => a - b);
      if (nextValues.length) return nextValues[0];
      else return "";
    }
  });

  return result;
}
