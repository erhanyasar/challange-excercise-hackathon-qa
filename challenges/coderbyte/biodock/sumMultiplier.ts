/**
 * Sum Multiplier
 * 
 * Have the function `SumMultiplier(arr)` take the array of numbers
 * stored in `arr` and return the string 'true' if any two numbers
 * can be multiplied so that the answer is greater than double the
 * sum of all the elements in the array. If not, return the string
 * 'false'.
 * 
 * For example, if `arr` is [2, 5, 6, -6, 16, 2, 3, 6, 5, 3] then
 * the sum of all these elements is 42, doubling 84. There
 * are two elements in the array, 16 * 6 = 96 is greater than 84,
 * so your program should return the string 'true'.
 * 
 * Examples:
 * ---------
 * Input: [2, 2, 2, 2, 4, 1]
 * Output: 'false'
 * 
 * Input: [1, 1, 2, 10, 3, 1, 12]
 * Output: 'true'
 * 
 * @param {number[]} arr
 * @returns {string}
 */

/**
 * Time complexity: O(n log n)
 * Space complexity: O(n) - It's possible to have O(1) without creating extra _arr
 */
function SumMultiplier (arr: number[]) {
    const _arr = arr.slice(),
        n = arr.length,
        doubledSum = arr.reduce((acc, val) => acc + val) * 2;

    _arr.sort((a, b) => a - b);

    let greaterMultiplication = Math.max(_arr[n-1] * _arr[n-2], _arr[0] * _arr[1])

    return greaterMultiplication > doubledSum ? 'true' : 'false';
}

console.log(SumMultiplier([2, 2, 2, 2, 4, 1]));
console.log(SumMultiplier([1, 1, 2, 10, 3, 1, 12]));