/*
 * Histogram Area
 *
 * Function histogramArea(arr) reads the array of non-negative integers stored in parameter `arr` which will represent the
 * heights of bars on a graph (where each bar width is 1), and determine the largest area underneath the entire bar graph.
 *
 * For example: if arr is [2, 1, 3, 4, 1] then the area of that space is equal to 6 because the entire width is 2 and the
 * maximum height is 3, therefore 2 * 3 = 6. Your program should return 6. The array will always contain at least 1 element.
 *
 * Examples;
 *
 * Input: [6, 3, 1, 4, 12, 4]
 * Output: 12
 *
 * Input: [5, 6, 7, 4, 1]
 * Output: 16
 * 
 * @param {number[]} arr
 * @returns {number}
 */

function histogramArea(arr) { 
  let result = 0,
    stack = [],
    index = 0;
    arrLength = arr.length;

  function findMax() {
    let top = stack.pop(),
      height = arr[top],
      width = stack.length === 0 ? index : index - stack[stack.length - 1] - 1,
      area = width * height;

    result = Math.max(result, area);
  }

  while (index < arrLength) {
    if (stack.length === 0 || arr[index] >= arr[stack[stack.length - 1]])
      stack.push(index++);
    else findMax();
  };

  while (stack.length > 0)
    findMax();

  return result;
}
   
console.log(histogramArea([2, 1, 3, 4, 1]));