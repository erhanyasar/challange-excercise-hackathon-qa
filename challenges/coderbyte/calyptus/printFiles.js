/*
 * Node.js Print Files
 *
 * First, create a file in the current directory with the name newFile.txt filled with any content. * 
 * Then, print all the files to the console using exec in the current directory so that they are in the following format: FILENAME, FILENAME, ...
 */

const fs = require('fs');
const exec = require('child_process').exec; 

// create file called newFile.txt
fs.writeFileSync('newFile.txt', 'New file content');

// then print contents of directory according to instructions above
exec('ls', (error, stdout, stderr) => {
  if (error) console.error(`exec error: ${error}`);
  if (stderr) console.error(`stderr: ${stderr}`);

  const files = stdout.trim().split('\n').join(', ');

  console.log(files);
});