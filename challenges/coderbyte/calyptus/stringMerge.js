/*
 * String Merge
 *
 * Function stringMerge(str) takes `str` parameter which will contain a large string of alphanumeric characters
 * with a single asterisk character splitting the string evenly into two separate strings. Your goal is to return
 * a new string by pairing up the characters in the corresponding locations in both strings.
 *
 * For example: if `str` is "abc1*kyoo" then `stringMerge()` should return the string "akbyco1o". Because 'a' pairs with 'k',
 * 'b' pairs with 'y', etc. The string will always split evenly with the asterisk in the center.
 *
 * Examples;
 *
 * Input: "aaa*bbb"
 * Output: "ababab"
 * 
 * Input: "123hg*aaabb"
 * Output: "1a2a3ahbgb"
 *
 * @param {string} str
 * @returns {string}
 */

function stringMerge(str) { 
  let result = '',
    asteriskPosition = Math.ceil(str.length / 2);

  for (let i=0; i<asteriskPosition - 1; i++)
    result += `${str[i]}${str[asteriskPosition + i]}`;

  return result; 
}
   
console.log(stringMerge("aaa*bbb"));