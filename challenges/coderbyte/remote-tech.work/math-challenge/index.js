function MathChallenge(str) { 
  let _str = (' ' + str).slice(1),
    result,
    arithmeticOperator,
    firstOperand,
    secondOperand,
    thirdOperand,
    index,
    xPosition;

  index = _str.indexOf(" ");
  firstOperand = _str.slice(0, index);

  arithmeticOperator = _str[index + 1];
  index = _str.indexOf("=");

  secondOperand = _str.slice(_str.indexOf(arithmeticOperator) + 2, index);
  thirdOperand = _str.slice(index + 2);

  const findX = (_result) => {
    let operand = xPosition === 3 ? thirdOperand : xPosition === 2 ? secondOperand : firstOperand;

    _result = (" " + _result).slice(1);
    index = operand.indexOf('x');

    return _result[index];
  }

  const add = () => {
    if (xPosition === 3) {
      result = parseInt(thirdOperand) - parseInt(secondOperand);

      if (thirdOperand.length > 1) return findX(result);
    } else if (xPosition === 2) {
      result = parseInt(thirdOperand) - parseInt(firstOperand);

      if (secondOperand.length > 1) return findX(result);
    } else if (xPosition === 1) {
      result = parseInt(firstOperandparseInt) + parseInt(secondOperandparseInt);

      if (firstOperand.length > 1) return findX(result);
    }

    return result;
  }

  const subtract = () => {  
    if (xPosition === 3) {
      result = parseInt(firstOperand) - parseInt(secondOperand);

      if (thirdOperand.length > 1) return findX(result);
    } else if (xPosition === 2) {
      result = parseInt(firstOperand) - parseInt(thirdOperand);

      if (secondOperand.length > 1) return findX(result);
    } else if (xPosition === 1) {
      result = parseInt(firstOperand) + parseInt(thirdOperand);

      if (firstOperand.length > 1) return findX(result);
    }

    return result;
  }

  const divide = () => {
    if (xPosition === 3) {
      result = parseInt(thirdOperand) / parseInt(secondOperand);

      if (thirdOperand.length > 1) return findX(result);
    } else if (xPosition === 2) {
      result = parseInt(thirdOperand) - parseInt(firstOperand);

      if (secondOperand.length > 1) return findX(result);
    } else if (xPosition === 1) {
      result = parseInt(firstOperand) + parseInt(secondOperand);

      if (firstOperand.length > 1) return findX(result);
    }

    return result;
  }

  const multiply = () => {
    if (xPosition === 3) {
      result = parseInt(thirdOperand) * parseInt(secondOperand);

      if (thirdOperand.length > 1) return findX(result);
    } else if (xPosition === 2) {
      result = parseInt(thirdOperand) / parseInt(firstOperand);

      if (secondOperand.length > 1) return findX(result);
    } else if (xPosition === 1) {
      result = parseInt(thirdOperand) / parseInt(secondOperand);

      if (firstOperand.length > 1) return findX(result);
    }

    return result;
  }

  if (firstOperand.includes('x')) xPosition = 1;
  if (secondOperand.includes('x')) xPosition = 2;
  if (thirdOperand.includes('x')) xPosition = 3;

  switch (arithmeticOperator) {
    case '+': return add();
    case '-': return subtract();
    case '/': return divide();
    case '*': return multiply();
  }
}
   
// keep this function call here 
console.log(MathChallenge(readline()));