const fs = require("fs");
const util = require("util");
const exec = util.promisify(require("child_process").exec);

// create file called newfile.txt
fs.writeFile("newfile.txt", "created by Erhan", function (err) {
  if (err) throw err;
});

// then print contents of directory according to instructions above
const main = async () => {
  const { stdout, stderr } = await exec("ls -xm", ["a", "-n"]);

  if (stderr) throw err;
  else console.log(stdout);
};

main();
