function HTMLElements(str) {
  let result = true,
    domArray = [],
    domElements = ["<b>", "<i>", "<em>", "<div>", "<p>"],
    domElementPairs = ["</b>", "</i>", "</em>", "</div>", "</p>"],
    strToProcess = str,
    firstTagIndex = -1;

  domElements.forEach((element) => {
    if (strToProcess.startsWith("</")) {
      domElementPairs.forEach((pair) => {
        if (domArray[domArray.length - 1] === pair) {
          domArray.pop();
          strToProcess = strToProcess.substring(pair.length, strToProcess.length);
        }
      });
    } else if (strToProcess.startsWith("<")) {
      if (strToProcess.startsWith(element)) {
        domArray.push(element);
        strToProcess = strToProcess.substring(element.length, strToProcess.length);
      }
    } else if (strToProcess === "") {
      result = true;
    } else {
      firstTagIndex = strToProcess.indexOf("<");
      strToProcess = strToProcess.substring(element.length, strToProcess.length);
    }
  });

  return result;
}
