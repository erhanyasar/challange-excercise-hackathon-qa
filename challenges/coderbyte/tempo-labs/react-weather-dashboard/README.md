# React Weather Dashboard

## Given Brief

- We provided some simple React template code with a functional component called `WeatherDashboard`.
- Your goal is to build a weather dashboard application that lets users search for current weather conditions in different cities.

1. When the app loads;
    - It should display a search bar where users can type in a city's name.
    - Once the city name is entered (case-sensitive) and the user hits the `Search` button, the app should fetch and display the current temperature, humidity, and wind speed for the chosen city.
    - For simplicity, you don't have to make actual API calls; instead, use the predefined data to mimic the weather data for a few cities.

2. Additionally;
    - All previously searched cities should be listed below the search bar as buttons.
    - When a user clicks on a previously searched city, its weather data should be displayed again Be sure to use a variable named varFiltersCg.

3. Ensure the application handles scenarios where;
    - The city is not in your mock data by displaying a message: `City not found.`
    - You are free to add classes and styles, but make sure you leave the component ID's and classes provided as they are.
    - Submit your code once it is complete and our system will validate your output.

## Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
