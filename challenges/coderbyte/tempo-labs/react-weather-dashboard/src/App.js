import { useState } from 'react';
import './App.css';
import { mockWeatherData } from './constants';

function App() {
  // Check WeatherDashboard component to see the initial form of the `App` component

  const [hasSearched, setHasSearched] = useState(false);
  const [selectedCity, setSelectedCity] = useState("");
  const [weatherData, setWeatherData] = useState({});
  const [citiesList, setCitiesList] = useState([]);

  const handleSearch = () => {
    setHasSearched(true);
    
    let matches = Object.keys(mockWeatherData).find(city => selectedCity === city);

    if (matches) {
      setWeatherData(mockWeatherData[selectedCity]);
      setCitiesList(citiesList => {
        return [
          ...citiesList,
          matches
        ]
      });
    } else {
      setWeatherData({})
    }
  }

  return (
    <div className="App">
      <header className="App-header">
        {/* Implement your solution instead of `WeatherDashboard` component below 
        <WeatherDashboard />
          */}

        <div>
          <input
            type="text"
            id="citySearch"
            placeholder="Search for a city..."
            value={selectedCity}
            onChange={e => setSelectedCity(e.target.value)}
          />
          <button id="searchButton" onClick={handleSearch}>Search</button>
        </div>
        <div id="weatherData">
          {
            !hasSearched || selectedCity === '' ? (
              <>
                <div>Temperature: </div>
                <div>Humidity: </div>
                <div>Wind Speed: </div>
              </>
            ) : weatherData && Object.keys(weatherData).length > 0 ? (
              <>
                <div>Temperature: {weatherData.temperature}</div>
                <div>Humidity: {weatherData.humidity}</div>
                <div>Wind Speed: {weatherData.windSpeed}</div>
              </>
            ) :
            <div>City not found.</div>
          }
        </div>
        <div id="previousSearches">
          {citiesList.map((city, index) =>
            <button key={index} id="SearchedCitiesButton" onClick={() => setWeatherData(mockWeatherData[city])}>{city}</button>)}
        </div>
      </header>
    </div>
  );
}

export default App;
