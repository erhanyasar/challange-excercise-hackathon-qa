/**
 * Given the function ArrayChallenge(strArr) read the array of strings stored in strArr,
 * which will contain 2 elements: the first element will be a sequence of characters representing a word,
 * and the second element will be a long string of comma-separated words, in alphabetical order,
 * that represents a dictionary of some arbitrary length.
 *
 * For example: strArr can be: ["worlcde", "apple,bat,cat,goodbye,hello,yellow,why,world"]. Your goal is
 * to determine the minimum number of characters, if any, can be removed from the word so that it matches
 * one of the words from the dictionary. In this case, your program should return 2 because once you remove
 * the characters "c" and "e" you are left with "world" and that exists within the dictionary. If the word
 * cannot be found no matter what characters are removed, return -1.
 *
 * @param {*} strArr
 * @returns {integer}
 */

function ArrayChallenge(
  strArr = ["baseball", "a,all,b,ball,bas,base,cat,code,d,e,quit,z"] // ["apbpleeeef", "a,ab,abc,abcg,b,c,dog,e,efd,zzzz"]
  // TODO: It doesn't accomplish fully answer since it works only full matching of the phrase inside the target word
) {
  let minElementsToRemove = strArr[0].length,
    stringsToMatchArr = strArr[1].split(","),
    matchesArr = [],
    maxLength;

  for (let i = 0; i < stringsToMatchArr.length; i++) {
    if (strArr[0].indexOf(stringsToMatchArr[i]) !== -1) matchesArr.push(stringsToMatchArr[i]);
  }

  matchesArr.sort((a, b) => a.length - b.length);
  maxLength = matchesArr[matchesArr.length - 1].length;
  minElementsToRemove = strArr[0].length - maxLength;

  return minElementsToRemove !== strArr[0].length ? minElementsToRemove : -1;
}

console.log(ArrayChallenge());
