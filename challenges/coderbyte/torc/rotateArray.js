/**
 * Given an array of numbers as a parameter to ArrayChallenge function below,
 * it's aimed to rotate the array as much as the first parameter of that array.
 *
 * For instance, given an array of [3, 1, 2, 4, 6] as a parameter, the array
 * should be rotated 3 times which is resulting the arrays followed in the order;
 * [1, 2, 4, 6, 3], [2, 4, 6, 3, 1], and [4, 6, 3, 1 ,2] eventually.
 *
 * As a result, 46312 should be returned from the function.
 *
 * @param {*} arr
 * @returns {integer}
 */

function ArrayChallenge(arr = [3, 1, 2, 4, 6]) {
  let rotatedArr = arr.slice(),
    removedElement;

  for (let i = 0; i < arr[0]; i++) {
    removedElement = rotatedArr.shift();
    rotatedArr.push(removedElement);
  }

  return rotatedArr.join("");
}

console.log(ArrayChallenge());
