import React, { useState } from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';

const style = {
  table: {
    borderCollapse: 'collapse'
  },
  tableCell: {
    border: '1px solid gray',
    margin: 0,
    padding: '5px 10px',
    width: 'max-content',
    minWidth: '150px'
  },
  form: {
    container: {
      padding: '20px',
      border: '1px solid #F0F8FF',
      borderRadius: '15px',
      width: 'max-content',
      marginBottom: '40px'
    },
    inputs: {
      marginBottom: '5px'
    },
    submitBtn: {
      marginTop: '10px',
      padding: '10px 15px',
      border:'none',
      backgroundColor: 'lightseagreen',
      fontSize: '14px',
      borderRadius: '5px'
    }
  }
}

function PhoneBookForm({ addEntryToPhoneBook, user, setUser }) {

  return (
    <form onSubmit={e => { e.preventDefault() }} style={style.form.container}>
      <label>First name:</label>
      <br />
      <input 
        style={style.form.inputs}
        className='userFirstname'
        name='userFirstname' 
        type='text'
        value={user?.firstName}
        onChange={(e) => setUser(prevState => {
          return {
            ...prevState,
            firstName: e.target.value
          }
        })}
      />
      <br/>
      <label>Last name:</label>
      <br />
      <input 
        style={style.form.inputs}
        className='userLastname'
        name='userLastname' 
        type='text'
        value={user?.lastName}
        onChange={(e) => setUser(prevState => {
          return {
            ...prevState,
            lastName: e.target.value
          }
        })}
      />
      <br />
      <label>Phone:</label>
      <br />
      <input
        style={style.form.inputs}
        className='userPhone' 
        name='userPhone' 
        type='text'
        value={user?.phone}
        onChange={(e) => setUser(prevState => {
          return {
            ...prevState,
            phone: e.target.value
          }
        })}
      />
      <br/>
      <input 
        style={style.form.submitBtn} 
        className='submitButton'
        type='submit' 
        value='Add User'
        onClick={addEntryToPhoneBook}
      />
    </form>
  )
}

function InformationTable({ users }) {
  return (
    <table style={style.table} className='informationTable'>
      <thead> 
        <tr>
          <th style={style.tableCell}>First name</th>
          <th style={style.tableCell}>Last name</th>
          <th style={style.tableCell}>Phone</th>
        </tr>
      </thead>
      <tbody>
        {users?.map((user, index) => {
          return (
            <tr key={index}>
              <td style={style.tableCell}>{user.firstName}</td>
              <td style={style.tableCell}>{user.lastName}</td>
              <td style={style.tableCell}>{user.phone}</td>
            </tr>
          )
        })}
      </tbody>
    </table>
  );
}

function App(props) {
  const [users, setUsers] = useState([]);
  const [user, setUser] = useState({
    firstName: "Coder",
    lastName: "Byte",
    phone: "8885559999"
  });

  const handhandleUserSubmit = () => {
    setUsers(prevState => {
      let _unsortedUsers = prevState.slice();

      _unsortedUsers.push(user);
      _unsortedUsers.sort((a, b) => a.lastName.localeCompare(b.lastName));
      // a.lastName.toLowerCase() > b.lastName.toLowerCase() ? 1 : a.lastName.toLowerCase() < b.lastName.toLowerCase() ? -1 : 0
      return [ ..._unsortedUsers ];
    });
  }

  return (
    <section>
      <PhoneBookForm addEntryToPhoneBook={handhandleUserSubmit} user={user} setUser={setUser} />
      <InformationTable users={users} />
    </section>
  );
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
