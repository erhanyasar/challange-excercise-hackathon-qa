import React, { createContext, useContext, useReducer, useState } from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import reportWebVitals from "./reportWebVitals";

const languages = ["Javascript", "Python"];

const initialState = { lang: languages[0] };
const LanguageContext = createContext({ ...initialState });

function useLanguage() {
  return useContext(LanguageContext);
}

const reducer = (state, action) => {
  const { type, payload } = action;

  switch (type) {
    case "TOGGLE":
      return {
        ...state,
        lang: payload === languages[0] ? languages[1] : languages[0],
      };
    default:
      return state;
  }
};

const LanguageProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <LanguageContext.Provider value={{ state, dispatch }}>
      {children}
    </LanguageContext.Provider>
  );
};

function App() {
  return (
    <LanguageProvider>
      <MainWrapper />
    </LanguageProvider>
  );
}

function MainWrapper() {
  const { state, dispatch } = useLanguage();
  const [selectedLanguage, setSelectedLanguage] = useState(state.lang);

  const handleToggle = () => {
    setSelectedLanguage(
      selectedLanguage === languages[0] ? languages[1] : languages[0]
    );
    dispatch({ type: "TOGGLE", payload: selectedLanguage });
  };
  console.log(state);

  return (
    <>
      Language choosen:
      {selectedLanguage}
      <button onClick={handleToggle}>Toggle</button>
    </>
  );
}

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
