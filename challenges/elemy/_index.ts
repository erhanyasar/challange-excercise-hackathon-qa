export default function min_removal(num: Number, arr: number[]): number {
  let mex: number = -1,
    sortedArr: number[] = [],
    sortedSet = new Set<number>(),
    arrOfNumbers: number[] = [...Array(90).keys()],
    removalsMap = new Map<number, number>();

  sortedArr = arr.sort((a, b) => a - b);
  sortedArr.forEach((element) => sortedSet.add(element));

  // First find mex
  function findMex() {
    Array.from(sortedSet).forEach((element, index) => {
      if (element === arrOfNumbers[index]) {
        if (mex === -1 && index === Array.from(sortedSet).length - 1)
          mex = element + 1;
        else return;
      } else if (element > arrOfNumbers[index]) mex = arrOfNumbers[index];
    });
    return mex;
  }
  findMex();

  // Then check min removal
  function checkRemovals() {
    let count: number = 1;
    const filteredSortedArr: number[] = sortedArr.filter(
      (element) => element < mex
    );

    filteredSortedArr.forEach((element, index) => {
      let occurenceOfTheElement: number = filteredSortedArr.filter(
        (el) => element === el
      ).length;
      if (index === 0) count = occurenceOfTheElement;
      removalsMap.set(element, occurenceOfTheElement);
      if (count > occurenceOfTheElement) count = occurenceOfTheElement;
    });
    return count !== 0 ? count : -1;
  }

  return checkRemovals();
}

console.log(min_removal(5, [0, 0, 0, 1, 1, 1, 1]));
