function findMex(set, numbersArr, _mex) {
  Array.from(set).forEach((element, index) => {
    if (element === numbersArr[index]) {
      if (_mex === -1 && index === Array.from(set).length - 1)
        _mex = element + 1;
      else return;
    } else if (element > numbersArr[index]) _mex = numbersArr[index];
  });
  return _mex;
}

function checkRemovals(arr, map, _mex) {
  let count;
  const filteredSortedArr = arr.filter((element) => element < _mex);

  filteredSortedArr.forEach((element, index) => {
    let occurenceOfTheElement = filteredSortedArr.filter(
      (el) => element === el
    ).length;
    if (index === 0) count = occurenceOfTheElement;
    map.set(element, occurenceOfTheElement);
    if (count > occurenceOfTheElement) count = occurenceOfTheElement;
  });
  return count !== 0 ? count : -1;
}

function min_removal(arr) {
  let mex = -1,
    sortedArr = [],
    sortedSet = new Set(),
    arrOfNumbers = [...Array(90).keys()],
    removalsMap = new Map();

  sortedArr = arr.sort((a, b) => a - b);
  sortedArr.forEach((element) => sortedSet.add(element));

  // First find mex
  mex = findMex(sortedSet, arrOfNumbers, mex);

  // Then check min removal
  return checkRemovals(sortedArr, removalsMap, mex);
}

console.log(min_removal([0, 0, 0, 1, 1, 1, 1]));
// [0, 1, 1, 4]
