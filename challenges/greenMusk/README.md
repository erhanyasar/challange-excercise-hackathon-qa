In this test task we have a set of frameworks and tools that are required to be used. Here is the tech stack you need to follow:

Typescript Language
SvelteKit framework (Not Svelte itself, we require you to use Kit and use the server feature.)
TailwindCSS
Hasura Engine for GraphQL
Docker for containers
Vertical Slice Architecture

You can find the test UI in this [link](https://brandio.io/envato/iofrm/html/login2.html). Task requires you to implement the form in the given tech stack and then make an api call to save the user to the database. If a user registers with an existing email we should do proper error handling and show that user exists. One more constraint is that this platform should be for some existing user base. You will need to check if the user who is registering is already registered to some other platform. You can use FakeAPI for that. Here is the [link](https://jsonplaceholder.typicode.com/comments). 