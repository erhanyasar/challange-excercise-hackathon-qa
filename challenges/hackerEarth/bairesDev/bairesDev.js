function findIsPerfect(numbers = [3, 6, 5, 28]) {
  let isPerfectNumber = false;

  numbers.forEach((number) => {
    let divisors = (n = number) =>
      [...Array(n + 1).keys()]
        .slice(1)
        .reduce((s, a) => s + (!(n % a) && a), 0);
  });

  return isPerfectNumber ? "YES" : "NO";
}

console.log(findIsPerfect());
