/**
 * Two strings are anagrams if they're permutations of each other. In other words,
 * both strings have the same size and the same characters. For example, "aaagmnrs"
 * is an anagram of "anagrams".
 * 
 * Given an array of strings, remove each string that is an anagram of an earlier
 * string and then return the remaining array in sorted order.
 * 
 * @param {string[]} text 
 * @returns string[]
 */

function funWithAnagrams(text = ["code", "aaagmnrs", "anagrams", "doce"]) {
  let remainingStrings = text.slice(),
    keys1,
    keys2;

  for (let i = 0; i < remainingStrings.length - 1; i++) {
    for (let j = i + 1; j < remainingStrings.length; j++) {
      let equalityCounter = 0,
        temp1 = {},
        temp2 = {};

      [...remainingStrings[i]].forEach((element) => {
        temp1[`${element}`] =
          typeof temp1[`${element}`] !== "undefined"
            ? temp1[`${element}`] + 1
            : 1;
      });
      [...remainingStrings[j]].forEach((element) => {
        temp2[`${element}`] =
          typeof temp2[`${element}`] !== "undefined"
            ? temp2[`${element}`] + 1
            : 1;
      });

      keys1 = Object.keys(temp1).sort();
      keys2 = Object.keys(temp2).sort();

      for (let k = 0; k < keys1.length; k++) {
        if (temp2[keys1[k]] === temp1[keys1[k]]) equalityCounter++;
        if (equalityCounter === keys1.length) remainingStrings.splice(j, 1);
      }
    }
  }

  const sortedRemainingStrings = remainingStrings.sort((a, b) => a < b ? -1 : a === b ? 0 : 1);

  return sortedRemainingStrings;
}

console.log(funWithAnagrams());
