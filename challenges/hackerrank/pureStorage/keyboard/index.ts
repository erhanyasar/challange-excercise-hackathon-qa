const matrixLogic = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6],
];

function entryTime(s: string, keypad: string) {
  let firstRow: number[] = [],
    secondRow: number[] = [],
    thirdRow: number[] = [];
  const distancesArray: number[] = [],
    mappedKeypad = new Map(),
    lengthOfTheString: number = s.length,
    indexesOfStartingPoint: number[] = [];

  /*
  // build keypad logic to check
  Array.from(keypad).forEach((element, index) => {
    if ([0, 1, 2].includes(index)) firstRow.push(Number(element));
    else if ([3, 4, 5].includes(index)) secondRow.push(Number(element));
    else if ([6, 7, 8].includes(index)) thirdRow.push(Number(element));
  });
  const keypadArray = [[...firstRow], [...secondRow], thirdRow];
  keypadArray.forEach((arr, i) => {
    arr.forEach((element, j) => {
      if (element === Number(s[0])) indexesOfStartingPoint.push(i, j);
    });
  });
  */
  // console.log(indexesOfStartingPoint);
  const [a] = keypad;
  console.log(a);

  mappedKeypad.forEach((key, value) => {
    console.log(key, value);
  });

  // check string s accordingly
  for (let i = 1; i < lengthOfTheString; i++) {
    if (s[i] === s[i - 1]) distancesArray.push(0);
  }

  return distancesArray.reduce((a, b) => a + b, 0);
}

console.log(entryTime("423692", "923857614"));
// 5111 752961348
