/*
 * Common Prefix
 *
 * Imagine a document version control system where each version is represented by a string of
 * lowercase letters. Given an array of version strings, write a function that needs to find
 * the longest common prefix among all versions of a document. If there is no, return an empty string.
 *
 * Examples;
 * 
 * Input: ["version1a", "version1b", "version1c"]
 * Output: "version1"
 *
 * Input: ["doc", "document", "documentation"]
 * Output: "doc"
 * 
 * Requirements;
 * - Function should take an array of strings as input
 * - All strings contain only lowercase English letters
 * - Return the longest common prefix as a string
 * - If no common prefix exists, return empty string
 * - Handle cases where input array length varies from 1 to 200
 * - Individual string length can be between 0 and 200 characters
 * 
 * @param {string[]} versionsArr
 * @returns {string}
 */

function commonPrefix(versionsArr) {
    let _arr = versionsArr.slice(),
        arrLength = versionsArr.length,
        counter = 1,
        result = '';

    _arr.sort();

    for (let i=1; i<arrLength; i++) {
        if (counter === 1 && !_arr[i].includes(_arr[0])){
            _arr[0] = _arr[0].slice(0, -1);
            i=0;
        }
        else if (_arr[i].includes(_arr[0])) counter++;
    }

    if (counter === arrLength) result = _arr[0];

    return result;
}

console.log(commonPrefix(['doc', 'document', 'documentation']));
console.log(commonPrefix(['documentation', 'document', 'doc']));
console.log(commonPrefix(['version1a', 'version1b', 'version1c']));