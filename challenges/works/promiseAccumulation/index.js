'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', function(inputStdin) {
    inputString += inputStdin;
});

process.stdin.on('end', function() {
    inputString = inputString.split('\n');

    main();
});

function readLine() {
    return inputString[currentLine++];
}

const promiseReturn = (a) => {
    const negatives = [93, 69, 72];
    if(negatives.includes(a)){
        return Promise.reject();
    }
    return Promise.resolve(a);
}

/*
 * Complete the 'promiseAccumulation' function below.
 *
 * The function is expected to yield a number.
 */

function* promiseAccumulation(promiseArr) {
    promiseArr.forEach(promise => {
        try {
            if (Promise.allSettled(promiseArr))
            yield promise;
        } catch (err) {
            console.error(err);
            yield -1;
        }
    })
    return promiseArr
}

async function main() {
    const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

    let n = parseInt(readLine().trim());
    let pArr = [];
    for(let i = 0;i<n;i++){
        let x = parseInt(readLine().trim());
        pArr.push(promiseReturn(x));
    }
    const gen = promiseAccumulation(pArr);
    for(let i = 0;i<n;i++){
        let result = (await gen.next()).value;
        ws.write(result +'\n');
        if(result === -1){
            break;
        }
    }

    ws.end();
}