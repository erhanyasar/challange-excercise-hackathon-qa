/*
"use strict";

const fs = require("fs");

process.stdin.resume();
process.stdin.setEncoding("utf-8");

let inputString = "";
let currentLine = 0;

process.stdin.on("data", function (inputStdin) {
  inputString += inputStdin;
});

process.stdin.on("end", function () {
  inputString = inputString.split("\n");

  main();
});

function readLine() {
  return inputString[currentLine++];
}

/*
 * Complete the 'findEquivalentMagnitude' function below.
 *
 * The function is expected to return an INTEGER_ARRAY.
 * The function accepts following parameters:
 *  1. WEIGHTED_INTEGER_GRAPH unit
 *  2. INTEGER x
 */

/*
 * For the weighted graph, <name>:
 *
 * 1. The number of nodes is <name>Nodes.
 * 2. The number of edges is <name>Edges.
 * 3. An edge exists between <name>From[i] and <name>To[i]. The weight of the edge is <name>Weight[i].
 *
 */

function findEquivalentMagnitude(unitNodes, unitFrom, unitTo, unitWeight, x) {
  let resultArray = new Array();

  for (let index = 0; index < unitNodes; index++) {
    if (index === 0) resultArray.push(x);
    else if (index + 1 !== undefined)
      resultArray.push(Number(x * unitWeight[index + 1]));
    else resultArray.push(Number(x * unitWeight[index]));
  }

  return resultArray;
}

console.log(findEquivalentMagnitude(3, [1, 2], [2, 3], [2, 1], 1));
/*
function main() {
  const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

  const unitNodesEdges = readLine().split(" ");

  const unitNodes = parseInt(unitNodesEdges[0], 10);
  const unitEdges = parseInt(unitNodesEdges[1], 10);

  let unitFrom = [];
  let unitTo = [];
  let unitWeight = [];

  for (let i = 0; i < unitEdges; i++) {
    const unitFromToWeight = readLine().split(" ");

    unitFrom.push(parseInt(unitFromToWeight[0], 10));
    unitTo.push(parseInt(unitFromToWeight[1], 10));
    unitWeight.push(parseInt(unitFromToWeight[2], 10));
  }

  const x = parseInt(readLine().trim(), 10);

  const result = findEquivalentMagnitude(
    unitNodes,
    unitFrom,
    unitTo,
    unitWeight,
    x
  );

  ws.write(result.join("\n") + "\n");

  ws.end();
}
*/
