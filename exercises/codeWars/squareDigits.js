/**
 * You are asked to square every digit of a number and concatenate them.
 * 
 * Example #1, if we run 9119 through the function, 811181 will come out,
 * because 92 is 81 and 12 is 1. (81-1-1-81)
 * 
 * Example #2: An input of 765 will/should return 493625 because 72 is 49,
 * 62 is 36, and 52 is 25. (49-36-25)
 *
 * @param {number} num 
 * @returns {number}
 */

function squareDigits(num = 3212){
    let sum,
        squares = [],
        toStr = num.toString();
  
  for (let i=0; i<toStr.length; i++)
    squares.push(toStr[i] * toStr[i]);

  /* Either below `for` loop or `forEach` works pretty same
  for (let i=0; i<squares.length; i++)
    sum = sum ? `${sum}${squares[i]}` : squares[i];
  */

    squares.forEach((value, index) => sum = sum ? `${sum}${squares[index]}` : squares[index]);

    return parseInt(sum);
}

console.log(squareDigits());