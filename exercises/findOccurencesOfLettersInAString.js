/*  Write a function that takes a string as a parameter and return the occurences of each
letter as an object and represents counts for each letter as key value properties. */

const findOccurencesOfLettersInAString = (str = "hey what are you doing here?") => {
  const result = {};

  for (const s of str.split("")) if (s !== " ") result[s] = (result[s] || 0) + 1;

  return result;
};

console.log(findOccurencesOfLettersInAString());
