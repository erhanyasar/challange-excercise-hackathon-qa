/** 
 * Function `birthdayCakeCandles` should accept a numbers array
 * and returns a number. See `readme.md` for more info.
 * 
 * @param {number[]} candles 
 * @returns {number}
 */

function birthdayCakeCandles(candles: number[] = [3, 2, 1, 3]): number {
    let result=1,
        _candles = candles.slice(),
        arrLength = _candles.length,
        sortedCandles: Array<number> = [];

    sortedCandles = _candles.sort((a, b) => a - b).reverse();

    for (let i=1; i<arrLength; i++)
        if (sortedCandles[i] === sortedCandles[i-1]) result++;

    return result;
}

console.log(birthdayCakeCandles());