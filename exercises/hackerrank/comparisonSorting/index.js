/** 
 * Function `countingSort` accepts a numbers array and
 * returns numbers array. See `readme.md` for more info. 
 * 
 * @param {number[]} arr 
 * @returns {number[]}
 */

function countingSort(arr) {
  let frequencyArr = Array(100).fill(0);

  arr.forEach((element) => {
    frequencyArr[element] += 1;
  });

  return frequencyArr;
}

console.log(countingSort([]));
