/** 
 * Function `diagonalDifference` should accept an array of numbers array
 * and returns a number. See `readme.md` for more info.
 * 
 * @param {number[][]} candles 
 * @returns {number}
 */

function diagonalDifference(arr = [
  [1,2,3],
  [4,5,6],
  [9,8,9],
]) {
  let ltrDiagonal = 0,
    rtlDiagonal = 0;

  for (let i = 0; i < arr.length; i++) {
    ltrDiagonal += arr[i][i];
    rtlDiagonal += arr[i][arr.length - 1 - i];
  }

  return Math.abs(ltrDiagonal - rtlDiagonal);
}

console.log(diagonalDifference([
  [-1, 1, -7, -8],
  [-10, -8, -5, -2],
  [0, 9, 7, -1],
  [4, 4, -2, 1],
]));