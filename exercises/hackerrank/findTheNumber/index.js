/** 
 * Function `findNumber` should accept an array of numbers array
 * and returns a number. See `readme.md` for more info.
 * 
 * @param {number[]} arr
 * @param {number} k 
 * @returns {'YES' | 'NO'}
 */

const findNumber = (arr, k) => arr.includes(k) ? "YES" : "NO";