function processData(input) {
  let zigZagSequence = input,
    arrLength = input.length,
    k = (arrLength + 1) / 2,
    temp;

  zigZagSequence = input.sort((a, b) => a - b);
  /*
  for (let i = 0; i < k; i++) {
    temp = input[k - 1];
    input[k - 1] = input[arrLength - 1];
    input[input.length - 1] = temp;
  }
  */
  temp = input[k - 1];
  input[k - 1] = input[arrLength - 1];
  input[arrLength - 1] = temp;

  temp = input[1];
  input[1] = input[arrLength - 1];
  input[arrLength - 1] = temp;

  temp = input[2];
  input[2] = input[1];
  input[1] = temp;

  temp = input[2];
  input[2] = input[5];
  input[5] = temp;

  return zigZagSequence;
}

console.log(processData([1, 2, 3, 4, 5, 6, 7]));
/*
[1, 2, 3, 7, 5, 6, 4]
[1, 4, 3, 7, 5, 6, 2]
[1, 3, 4, 7, 5, 6, 2]
[1, 3, 6, 7, 5, 4, 2]
*/
