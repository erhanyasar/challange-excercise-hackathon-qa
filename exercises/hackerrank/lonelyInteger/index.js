/** 
 * Function `findNumber` should accept an array of numbers array
 * and returns a number. See `readme.md` for more info.
 * 
 * @param {number[]} arr
 * @param {number} k 
 * @returns {'YES' | 'NO'}
 */

function lonelyInteger(a=[0, 0, 1, 2, 1]) {
  let isEqual;

  if (a.length <= 1) return a[0];

  for (let i = 0; i < a.length; i++) {
    isEqual = false;

    for (let j = 0; j < a.length; j++) {
      if (i === j) continue;
      if (a[i] === a[j]) isEqual = true;
    }
    if (!isEqual) return a[i];
  }
}

console.log(lonelyInteger());