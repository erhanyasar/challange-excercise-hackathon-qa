/**
 * 
 * @param arr numbers array
 * @returns void logging smallest sum and greatest sum with a whitespace between
 */

function miniMaxSum(arr: number[]): void {
    let sumsArr: number[]=[],
        arrLength = arr.length;

    for (let i=0; i<arrLength; i++) {
        let sum=0;
        for (let j=0; j<arrLength; j++) {
            if (i === j) continue;
            sum += arr[j]
        }
        sumsArr.push(sum);
    }

    const sortedSumsArr = sumsArr.sort((a, b) => a - b);
    console.log(sortedSumsArr[0], sortedSumsArr[arrLength - 1]);
}

miniMaxSum([1, 2, 3, 4, 5]);