/**
 * If we list all the natural numbers below that are multiples of 3 or 5,
 * we get 3, 5, 6, and 9. The sum of these multiples is 23.
 * Find the sum of all the multiples of 3 or 5 below n.
 * @param {*} t
 * @param {*} n
 * @returns integer
 */

function multipliesOf3And5(t = 2, n = [10, 100]) {
  let counterArr = Array(t).fill(0);

  for (let i = 0; i < t; i++) {
    for (let j = 0; j < n[i]; j++) {
      if (j % 3 === 0 || j % 5 === 0) counterArr[i] += j;
    }
  }

  return counterArr.join("\n");
}

console.log(multipliesOf3And5());
