/**
 * If we list all the natural numbers below that are multiples of 3 or 5,
 * we get 3, 5, 6, and 9. The sum of these multiples is 23.
 * Find the sum of all the multiples of 3 or 5 below n.
 * @param {*} t
 * @param {*} n
 * @returns {integer}
 */

function createFibonacciArray(limit) {
  let _fibonacciArr = [1, 2],
    nextNumber =
      _fibonacciArr[_fibonacciArr.length - 1] +
      _fibonacciArr[_fibonacciArr.length - 2];

  while (nextNumber < limit) {
    _fibonacciArr.push(nextNumber);
    nextNumber =
      _fibonacciArr[_fibonacciArr.length - 1] +
      _fibonacciArr[_fibonacciArr.length - 2];
  }
  return _fibonacciArr;
}

function oddsFibonacciSum(t = 2, n = [10, 100]) {
  let counterArr = Array(t).fill(0),
    fibonacciArr = [];

  for (let i = 0; i < t; i++) {
    fibonacciArr = createFibonacciArray(n[i]);
    for (let j = 0; j < fibonacciArr.length; j++)
      if (fibonacciArr[j] % 2 === 0) counterArr[i] += fibonacciArr[j];
  }

  return counterArr.join("\n");
}

console.log(oddsFibonacciSum());
