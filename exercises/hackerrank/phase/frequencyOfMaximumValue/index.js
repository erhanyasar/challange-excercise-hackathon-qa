const frequencyOfMaxValue = (numbers = [2, 1, 2], q = [1, 2, 3]) => {
    let result = [],
        arrLength = numbers.length,
        _numbers = numbers.slice(),
        slicedNumbers;

    const findMaxFrequency = (param) => {
        let counter = 1,
            slicedNumbersLength;

        slicedNumbers = _numbers.slice(param);
        slicedNumbers.sort((a, b) => a - b).reverse();
        slicedNumbersLength = slicedNumbers.length;

        for (let j=0; j<slicedNumbersLength; j++) {
            if (slicedNumbers[j] === slicedNumbers[j + 1]) counter++;
            if (j === slicedNumbersLength - 1) result.push(counter);
        }
    }

    for (let i=0; i<arrLength; i++) findMaxFrequency(q[i] - 1);

    return result;
}

console.log(frequencyOfMaxValue());