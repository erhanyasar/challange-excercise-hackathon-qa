"use strict";

process.stdin.resume();
process.stdin.setEncoding("utf-8");

let inputString = "";
let currentLine = 0;

process.stdin.on("data", function (inputStdin) {
  inputString += inputStdin;
});

process.stdin.on("end", function () {
  inputString = inputString.split("\n");

  main();
});

function readLine() {
  return inputString[currentLine++];
}

/*
 * Complete the 'plusMinus' function below.
 *
 * The function accepts INTEGER_ARRAY arr as parameter.
 */

function plusMinus(arr) {
  let ratiosArray = [],
    arrLength = arr.length,
    positiveCounter = 0,
    negativeCounter = 0,
    zeroCounter = 0;

  arr.forEach((element) => {
    if (element > 0) positiveCounter++;
    else if (element < 0) negativeCounter++;
    else zeroCounter++;
  });

  ratiosArray = [
    (positiveCounter / arrLength).toFixed(6),
    (negativeCounter / arrLength).toFixed(6),
    (zeroCounter / arrLength).toFixed(6),
  ];

  ratiosArray.forEach((element) => {
    console.log(element);
  });
}

function main() {
  const n = parseInt(readLine().trim(), 10);

  const arr = readLine()
    .replace(/\s+$/g, "")
    .split(" ")
    .map((arrTemp) => parseInt(arrTemp, 10));

  plusMinus(arr);
}
