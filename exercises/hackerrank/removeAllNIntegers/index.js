process.stdin.resume();
process.stdin.setEncoding("utf-8");

var __input_stdin = "";
var __input_stdin_array = "";
var __input_currentline = 0;

process.stdin.on("data", function (data) {
  __input_stdin += data;
});

function LinkedListNode(node_value) {
  this.val = node_value;
  this.next = null;
}

function _insert_node_into_singlylinkedlist(head, tail, val) {
  if (head == null) {
    head = new LinkedListNode(val);
    tail = head;
  } else {
    var node = new LinkedListNode(val);
    tail.next = node;
    tail = tail.next;
  }
  return tail;
}

function _print_singly_linkedlist(head) {
  while (head) {
    process.stdout.write(head.val + " ");
    head = head.next;
  }
}

/*
 * Complete the function below.
 */
/*
For your reference:
LinkedListNode {
    var val;
    var next;
};
*/

function removeAll(N, linkedlist) {
  let elementsArray = [];

  while (linkedlist?.hasOwnProperty("next")) {
    if (linkedlist.val !== N) elementsArray.push(linkedlist.val);
    linkedlist = linkedlist.next;
  }

  if (elementsArray.length > 0) console.log(elementsArray.join(" "));
  else return;
}

process.stdin.on("end", function () {
  __input_stdin_array = __input_stdin.split(" ");
  var _N = parseInt(__input_stdin_array[__input_currentline].trim(), 10);
  __input_currentline += 1;

  var _linkedlist_size = 0;
  _linkedlist_size = parseInt(
    __input_stdin_array[__input_currentline].trim(),
    10
  );
  __input_currentline += 1;

  var _linkedlist_i, _linkedlist_item;
  var _linkedlist = null;
  var _linkedlist_tail = null;
  for (_linkedlist_i = 0; _linkedlist_i < _linkedlist_size; _linkedlist_i++) {
    var _linkedlist_item = parseInt(
      __input_stdin_array[__input_currentline].trim(),
      10
    );
    __input_currentline += 1;
    if (_linkedlist_i == 0) {
      _linkedlist = _insert_node_into_singlylinkedlist(
        _linkedlist,
        _linkedlist_tail,
        _linkedlist_item
      );
      _linkedlist_tail = _linkedlist;
    } else {
      _linkedlist_tail = _insert_node_into_singlylinkedlist(
        _linkedlist,
        _linkedlist_tail,
        _linkedlist_item
      );
    }
  }

  _linkedlist = removeAll(_N, _linkedlist);
  _print_singly_linkedlist(_linkedlist);
});
