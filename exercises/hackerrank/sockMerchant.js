"use strict";
/*
const fs = require("fs");

process.stdin.resume();
process.stdin.setEncoding("utf-8");

let inputString = "";
let currentLine = 0;

process.stdin.on("data", function (inputStdin) {
  inputString += inputStdin;
});

process.stdin.on("end", function () {
  inputString = inputString.split("\n");

  main();
});

function readLine() {
  return inputString[currentLine++];
}

/*
 * Complete the 'sockMerchant' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts following parameters:
 *  1. INTEGER n
 *  2. INTEGER_ARRAY ar
 */

function sockMerchant(n = 9, ar = [10, 20, 20, 10, 10, 30, 50, 10, 20]) {
  let pairCount = 0,
    isPair = false,
    pairsArr = [],
    tempCount = 0;

  for (let i = 0; i < n - 1; i++) {
    isPair = false;
    for (let j = i + 1; j < n; j++) {
      if (ar[i] === ar[j]) {
        pairCount++;

        if (j === i + 1) {
          isPair = true;
          tempCount = j;
        } else if (pairsArr.length) {
          for (let k = 0; k < pairsArr.length; k++) {
            if (pairsArr[k] === ar[i]) {
              pairsArr.splice(k, 1);
              pairCount--;
              i++;
            }
          }
        } else pairsArr.push(ar[j]);
        j = n - 1;
      }
    }
    if (isPair) i = tempCount;
  }
  return pairCount;
}

console.log(sockMerchant(10, [1, 1, 3, 1, 2, 1, 3, 3, 3, 3]));

/*
function main() {
  const ws = fs.createWriteStream(process.env.OUTPUT_PATH);
  const n = parseInt(readLine().trim(), 10);

  const ar = readLine()
    .replace(/\s+$/g, "")
    .split(" ")
    .map((arTemp) => parseInt(arTemp, 10));

  const result = sockMerchant(n, ar);

  ws.write(result + "\n");
  ws.end();
}
*/
