function sockMerchant(n, ar) {
    let pairCount = 0,
        paired = {};

    for (let i=0; i<n-1; i++) {
        for (let j=1; j<n; j++){
            
            if (i !== j && j > i && !paired[j] && !paired[i] && ar[i] === ar[j]) {
                pairCount++;
                paired[j] = true;
                break;
            }
        }
    }

    return pairCount;
}