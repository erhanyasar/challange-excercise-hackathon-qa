# Sock Merchant

There is a large pile of socks that must be paired by color. Given an array of integers representing the color of each sock, determine how many pairs of socks with matching colors there are.

## Function Description

sockMerchant has the following parameter(s):

    int n: the number of socks in the pile
    int ar[n]: the colors of each sock

Returns

    int: the number of pairs

### Input Format

- The first line contains an integer `n`, the number of socks represented in `arr`.
- The second line contains space-separated integers, `ar[i]`, the colors of the socks in the pile.

### Sample Input
n = 9
ar = [10, 20, 20, 10, 10, 30, 50, 10, 20]

### Sample Output
3