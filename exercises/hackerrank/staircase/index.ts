/**
 * @param n: number 
 * @returns void as logging staircase from hashes
 */

function staircase(n: number): void {
    let i=0;

    while (i<n) {
        let spaces="",
            hashes="";

        for (let j=i+1; j<n; j++) spaces += " ";
        for (let j=0; j<i+1; j++) hashes += "#";
        i++;
        console.log(spaces + hashes);
    }
}

staircase(9);