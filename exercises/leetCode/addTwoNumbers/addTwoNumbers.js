/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 */

const convertLinkedListToArray = (linkedList) => {
    let resultArray = [];

    resultArray.push(linkedList.val);
    while (linkedList.next) {
        linkedList = linkedList.next;
        resultArray.push(linkedList.val);
    }

    return resultArray;
}

const convertArrayToLinkedList = (arr) => {
    function ListNode(val, next) {
        this.val = (val===undefined ? 0 : val)
        this.next = (next===undefined ? null : next)
    }
    return arr.reverse().reduce((acc, curr) => {
        if (acc == null)
            acc = new ListNode(curr);
        else
            acc = new ListNode(curr, acc);
        return acc;
    }, null);
}

var addTwoNumbers = function(l1, l2) {
    let l1Arr = [],
        l2Arr;

    l1Arr = convertLinkedListToArray(l1);
    l2Arr = convertLinkedListToArray(l2);

    sumOfL1 = parseInt(l1Arr.reverse().join(""));
    sumOfL2 = parseInt(l2Arr.reverse().join(""));

    const resultArr = ((sumOfL1 + sumOfL2).toString().split("").reverse());

    return convertArrayToLinkedList(resultArr);
};