/**
 * @param {number} x
 * @return {boolean}
 */

var isPalindromeNumber = function(x) {
    if (x < 0) return false;
    else if (x == x.toString().split("").reverse().join(""))
        return true;
    else return false;
};