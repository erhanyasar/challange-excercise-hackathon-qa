/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */

var twoSum = function(nums, target) {
/*
 * Brute force
 * Time complexity: O(n2)
 * Space complexity: O(1)

    let indices = [];

    for (let i=0; i<nums.length - 1; i++) {
        for (let j=1; j<nums.length; j++) {
            if (i === j) continue;
            else if (nums[i] + nums[j] == target && indices.length === 0) {
                indices.push(i);
                indices.push(j);
            }
        }
    }

    return indices;
*/

/*
 * Improves Solution Complexity
 * Time complexity: O(n)
 * Space complexity: O(n)

    let map = new Map();

    for (let i=0; i<nums.length; i++)
        map.set(nums[i], i);

    for (let i=0; i<nums.length; i++) {
        const complement = target - nums[i];

        if (map.has(complement) && map.get(complement) !== i) return [i, map.get(complement)];

        return null;
    }
 */

    /* Refactored
     * Improves Solution Complexity
     * Time complexity: O(n)
     * Space complexity: O(n)
     */

    let map = new Map();

    for (let i=0; i<nums.length; i++) {
        const complement = target - nums[i];

        if (map.has(complement)) return [map.get(complement), i];
        map.set(nums[i], i);
    }

    return null;
};

console.log(twoSum([2, 7, 11, 15], 9));
// console.log(twoSum([3, 2, 4], 6));
// console.log(twoSum([3, 3], 6));