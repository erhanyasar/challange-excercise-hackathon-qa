/**
 * Write a function that accepts a string and returns the length for the longest non-repetitive
 * substring within given string.
 * 
 * @param {string} str
 * @returns string
 */

// Brute force solution with O(n)2 time complexity
function findLengthOfNonRepetitiveSubstring(str = "abcabcbbdd") {
  let strSet = new Set(),
    count = 0;

  function isRepeating(substr, index) {
    if (substr === str.substring(index, index + substr.length)) return true;
  }

  for (i = 0; i < str.length - 1; i++) {
    for (j = i + 1; j < str.length; j++) {
      if (j - 1 !== i && str[j] !== str[j - 1]) {
        if (str[i] !== str[j] && count !== -1) {
          strSet.add(str.substring(i, j + 1));
        } else if (isRepeating(str.substring(i, j), j)) {
          count = -1;
          j = 2 * j - 1;
        }
      }
    }
    i = str.length;
  }

  function findLongestWord() {
    let sortedSubstringsArray = Array.from(strSet).sort((a, b) => a - b);
    if (sortedSubstringsArray.length)
      return sortedSubstringsArray[sortedSubstringsArray.length - 1].length;
  }

  return findLongestWord();
}

console.log(findLengthOfNonRepetitiveSubstring());