/**
 * Write a function that accepts an array of numbers. It returns a boolean value that is TRUE
 * incase the array has at least one member equal to the sum of random 3 numbers from the array
 * and FALSE if it is not.
 * 
 * @param {[number]} array
 * @returns boolean
 */

// Brute force solution with O(n)3 time complexity
function hasSumOfNumbers(array = [0, 1, 2, -1, 5, -3, 4]) {
  for (let i = 0; i < array.length - 2; i++) {
    for (let j = i + 1; j < array.length - 1; j++) {
      for (let k = j + 1; k < array.length; k++) {
        console.log(array[i], array[j], array[k])
        if (array[i] + array[j] + array[k] === 0) return true;
      }
    }
  }
  return false;
}

console.log(hasSumOfNumbers());