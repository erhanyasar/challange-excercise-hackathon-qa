const STUDENTS = [
  {
    firstName: 'Emily',
    lastName: 'Johnson',
    address: {
      street: '123 Oak Street',
      city: 'Austin',
      state: 'TX',
      zip: '73301'
    },
    books: [
      '9780735219090',
      '9781982110567',
      '9781594634024',
      '9780679732242',
      '9780062024039'
    ]
  },
  {
    firstName: 'Michael',
    lastName: 'Smith',
    address: {
      street: '456 Maple Avenue',
      city: 'Los Angeles',
      state: 'CA',
      zip: '90001'
    },
    books: [
      '9780385737951',
      '9780385353304',
      '9781594634734',
      '9780451419439',
      '9780451419439',
      '9781451626650',
      '9780451526342'
    ]
  },
  {
    firstName: 'Jessica',
    lastName: 'Williams',
    address: {
      street: '789 Pine Lane',
      city: 'New York',
      state: 'NY',
      zip: '10001'
    },
    books: [ '9780735219090', '9780385490818', '9781594631931' ]
  },
  {
    firstName: 'James',
    lastName: 'Brown',
    address: {
      street: '101 Elm Drive',
      city: 'Chicago',
      state: 'IL',
      zip: '60601'
    },
    books: [ '9781250209764' ]
  },
  {
    firstName: 'Sarah',
    lastName: 'Jones',
    address: {
      street: '202 Cedar Road',
      city: 'Seattle',
      state: 'WA',
      zip: '98101'
    },
    books: [
      '9780061120084',
      '9781501180989',
      '9780060850524',
      '9780735212763'
    ]
  },
  {
    firstName: 'Christopher',
    lastName: 'Miller',
    address: {
      street: '303 Birch Street',
      city: 'Miami',
      state: 'FL',
      zip: '33101'
    },
    books: [ '9780142004234', '9780440180296' ]
  },
  {
    firstName: 'Amanda',
    lastName: 'Davis',
    address: {
      street: '404 Spruce Avenue',
      city: 'Denver',
      state: 'CO',
      zip: '80201'
    },
    books: [ '9780060934346', '9780316556347', '9780060850524' ]
  },
  {
    firstName: 'Joshua',
    lastName: 'Garcia',
    address: {
      street: '505 Aspen Lane',
      city: 'Phoenix',
      state: 'AZ',
      zip: '85001'
    },
    books: [
      '9780553213690',
      '9780307743657',
      '9780439358071',
      '9780061122415',
      '9780684801223',
      '9780141439846',
      '9780451524935',
      '9780399590504'
    ]
  },
  {
    firstName: 'Megan',
    lastName: 'Martinez',
    address: {
      street: '606 Walnut Drive',
      city: 'Portland',
      state: 'OR',
      zip: '97035'
    },
    books: [ '9781542040068' ]
  },
  {
    firstName: 'Daniel',
    lastName: 'Hernandez',
    address: {
      street: '707 Willow Road',
      city: 'Boston',
      state: 'MA',
      zip: '02101'
    },
    books: []
  },
  {
    firstName: 'Ashley',
    lastName: 'Lopez',
    address: {
      street: '808 Maple Street',
      city: 'Austin',
      state: 'TX',
      zip: '73301'
    },
    books: [
      '9780062024039',
      '9780684801223',
      '9781250301697',
      '9780399155345',
      '9780061120084'
    ]
  },
  {
    firstName: 'Matthew',
    lastName: 'Gonzalez',
    address: {
      street: '909 Oak Avenue',
      city: 'Los Angeles',
      state: 'CA',
      zip: '90001'
    },
    books: [
      '9780440180296',
      '9780451419439',
      '9781594634734',
      '9780399155345',
      '9780441172719'
    ]
  },
  {
    firstName: 'Brittany',
    lastName: 'Wilson',
    address: {
      street: '1010 Pine Lane',
      city: 'New York',
      state: 'NY',
      zip: '10001'
    },
    books: [
      '9780441172719',
      '9780679723165',
      '9780385737951',
      '9780684801223'
    ]
  },
  {
    firstName: 'Andrew',
    lastName: 'Anderson',
    address: {
      street: '1111 Elm Drive',
      city: 'Chicago',
      state: 'IL',
      zip: '60601'
    },
    books: [
      '9780142437223',
      '9780439785969',
      '9780375842206',
      '9781250080400',
      '9780441172719'
    ]
  },
  {
    firstName: 'Samantha',
    lastName: 'Thomas',
    address: {
      street: '1212 Cedar Road',
      city: 'Seattle',
      state: 'WA',
      zip: '98101'
    },
    books: [
      '9780061120084',
      '9781400031702',
      '9780679723165',
      '9781524763138',
      '9780142004234',
      '9780143035008'
    ]
  },
  {
    firstName: 'Ryan',
    lastName: 'Moore',
    address: {
      street: '1313 Birch Street',
      city: 'Miami',
      state: 'FL',
      zip: '33101'
    },
    books: [ '9781400079988', '9780393312836' ]
  },
  {
    firstName: 'Hannah',
    lastName: 'Taylor',
    address: {
      street: '1414 Spruce Avenue',
      city: 'Denver',
      state: 'CO',
      zip: '80201'
    },
    books: [
      '9780553213690',
      '9780439139601',
      '9780307588371',
      '9780684801223',
      '9780141439846',
      '9781250209764',
      '9780307588371',
      '9781503280786'
    ]
  },
  {
    firstName: 'Lauren',
    lastName: 'White',
    address: {
      street: '1616 Walnut Drive',
      city: 'Portland',
      state: 'OR',
      zip: '97035'
    },
    books: [
      '9780307387899',
      '9781451626650',
      '9780141439600',
      '9781451635621',
      '9780143039433'
    ]
  },
  {
    firstName: 'Joseph',
    lastName: 'Lee',
    address: {
      street: '1515 Aspen Lane',
      city: 'Phoenix',
      state: 'AZ',
      zip: '85001'
    },
    books: [
      '9781476738024',
      '9780307387899',
      '9780735212763',
      '9780142437261',
      '9780141439570',
      '9780316769488'
    ]
  },
  {
    firstName: 'Lauren',
    lastName: 'White',
    address: {
      street: '1616 Walnut Drive',
      city: 'Portland',
      state: 'OR',
      zip: '97035'
    },
    books: [ '9780743273565', '9780316055437', '9780307744432' ]
  },
  {
    firstName: 'Olivia',
    lastName: 'Adams',
    address: {
      street: '2828 Maple Street',
      city: 'Austin',
      state: 'TX',
      zip: '73301'
    },
    books: [ '9780141439600', '9780307588371' ]
  },
  {
    firstName: 'Brandon',
    lastName: 'Harris',
    address: {
      street: '1717 Willow Road',
      city: 'Boston',
      state: 'MA',
      zip: '02101'
    },
    books: [
      '9781503280786',
      '9781451626650',
      '9780061122415',
      '9780439136365',
      '9780393312836',
      '9780061120084'
    ]
  },
  {
    firstName: 'Rachel',
    lastName: 'Clark',
    address: {
      street: '1818 Maple Street',
      city: 'Austin',
      state: 'TX',
      zip: '73301'
    },
    books: [
      '9780062060627',
      '9780061120084',
      '9781451626650',
      '9780553213690',
      '9780307887443',
      '9780141439600'
    ]
  },
  {
    firstName: 'Tyler',
    lastName: 'Lewis',
    address: {
      street: '1919 Oak Avenue',
      city: 'Los Angeles',
      state: 'CA',
      zip: '90001'
    },
    books: [ '9780679732242', '9781542040068', '9780060883286' ]
  },
  {
    firstName: 'Nicole',
    lastName: 'Walker',
    address: {
      street: '2020 Pine Lane',
      city: 'New York',
      state: 'NY',
      zip: '10001'
    },
    books: []
  },
  {
    firstName: 'Dylan',
    lastName: 'Hall',
    address: {
      street: '2121 Elm Drive',
      city: 'Chicago',
      state: 'IL',
      zip: '60601'
    },
    books: [
      '9780061120084',
      '9780307744432',
      '9781451635621',
      '9780061122415',
      '9780061120084',
      '9780451526342',
      '9781451626650'
    ]
  },
  {
    firstName: 'Stephanie',
    lastName: 'Allen',
    address: {
      street: '2222 Cedar Road',
      city: 'Seattle',
      state: 'WA',
      zip: '98101'
    },
    books: [
      '9780142437223',
      '9780142437223',
      '9780590353427',
      '9781524763138',
      '9780060850524'
    ]
  },
  {
    firstName: 'Ethan',
    lastName: 'Young',
    address: {
      street: '2323 Birch Street',
      city: 'Miami',
      state: 'FL',
      zip: '33101'
    },
    books: [ '9780679732761' ]
  },
  {
    firstName: 'Alexandra',
    lastName: 'King',
    address: {
      street: '2424 Spruce Avenue',
      city: 'Denver',
      state: 'CO',
      zip: '80201'
    },
    books: [
      '9780399167065',
      '9780316055437',
      '9780439136365',
      '9780547928227',
      '9780679732761',
      '9780486280615',
      '9781400033416',
      '9780743273565',
      '9781524763138'
    ]
  },
  {
    firstName: 'Nathan',
    lastName: 'Wright',
    address: {
      street: '2525 Aspen Lane',
      city: 'Phoenix',
      state: 'AZ',
      zip: '85001'
    },
    books: [ '9780735219090' ]
  },
  {
    firstName: 'Victoria',
    lastName: 'Scott',
    address: {
      street: '2626 Walnut Drive',
      city: 'Portland',
      state: 'OR',
      zip: '97035'
    },
    books: [ '9780399590504' ]
  },
  {
    firstName: 'Benjamin',
    lastName: 'Green',
    address: {
      street: '2727 Willow Road',
      city: 'Boston',
      state: 'MA',
      zip: '02101'
    },
    books: [
      '9780316055437',
      '9780385737951',
      '9780307387899',
      '9781542040068',
      '9780385490818',
      '9780375842206',
      '9780679781585'
    ]
  },
  {
    firstName: 'Olivia',
    lastName: 'Adams',
    address: {
      street: '2828 Maple Street',
      city: 'Austin',
      state: 'TX',
      zip: '73301'
    },
    books: []
  },
  {
    firstName: 'Justin',
    lastName: 'Baker',
    address: {
      street: '2929 Oak Avenue',
      city: 'Los Angeles',
      state: 'CA',
      zip: '90001'
    },
    books: [
      '9780141439471',
      '9780439785969',
      '9781594634024',
      '9780066238500',
      '9780679723165',
      '9780141441146',
      '9780385543781',
      '9780525478812'
    ]
  },
  {
    firstName: 'Kimberly',
    lastName: 'Parker',
    address: {
      street: '4040 Pine Lane',
      city: 'New York',
      state: 'NY',
      zip: '10001'
    },
    books: [ '9780062797155' ]
  },
  {
    firstName: 'Stephanie',
    lastName: 'Allen',
    address: {
      street: '2222 Cedar Road',
      city: 'Seattle',
      state: 'WA',
      zip: '98101'
    },
    books: [
      '9780385490818',
      '9780679781585',
      '9780142004234',
      '9780385737951'
    ]
  },
  {
    firstName: 'Amber',
    lastName: 'Nelson',
    address: {
      street: '3030 Pine Lane',
      city: 'New York',
      state: 'NY',
      zip: '10001'
    },
    books: [ '9780142004234', '9780199535675', '9780140275360' ]
  },
  {
    firstName: 'Jonathan',
    lastName: 'Hill',
    address: {
      street: '3131 Elm Drive',
      city: 'Chicago',
      state: 'IL',
      zip: '60601'
    },
    books: [
      '9781451626650',
      '9780440180296',
      '9780684803357',
      '9780062060627',
      '9780486280615',
      '9780385353304',
      '9780805209990'
    ]
  },
  {
    firstName: 'Olivia',
    lastName: 'Adams',
    address: {
      street: '2828 Maple Street',
      city: 'Austin',
      state: 'TX',
      zip: '73301'
    },
    books: [ '9780141439471', '9780141439556' ]
  },
  {
    firstName: 'Brooke',
    lastName: 'Ramirez',
    address: {
      street: '3232 Cedar Road',
      city: 'Seattle',
      state: 'WA',
      zip: '98101'
    },
    books: [
      '9780545010221',
      '9781451626650',
      '9781400031702',
      '9780061120084'
    ]
  },
  {
    firstName: 'Anthony',
    lastName: 'Campbell',
    address: {
      street: '3333 Birch Street',
      city: 'Miami',
      state: 'FL',
      zip: '33101'
    },
    books: [
      '9780316055437',
      '9780060934346',
      '9780143058144',
      '9780439358071',
      '9780679720201',
      '9780141441672',
      '9780141439563',
      '9780439139601'
    ]
  },
  {
    firstName: 'Haley',
    lastName: 'Mitchell',
    address: {
      street: '3434 Spruce Avenue',
      city: 'Denver',
      state: 'CO',
      zip: '80201'
    },
    books: [
      '9780060934346',
      '9780385490818',
      '9780679732242',
      '9780062060627',
      '9780307887443',
      '9780142437261'
    ]
  },
  {
    firstName: 'Sydney',
    lastName: 'Evans',
    address: {
      street: '3838 Maple Street',
      city: 'Austin',
      state: 'TX',
      zip: '73301'
    },
    books: [
      '9780743273565',
      '9780141439471',
      '9780062024039',
      '9780307454546',
      '9781250209764'
    ]
  },
  {
    firstName: 'Jacob',
    lastName: 'Phillips',
    address: {
      street: '3737 Willow Road',
      city: 'Boston',
      state: 'MA',
      zip: '02101'
    },
    books: [ '9780451419439', '9780307387899' ]
  },
  {
    firstName: 'Kimberly',
    lastName: 'Parker',
    address: {
      street: '4040 Pine Lane',
      city: 'New York',
      state: 'NY',
      zip: '10001'
    },
    books: [
      '9780060850524',
      '9780451419439',
      '9780066238500',
      '9780553213690',
      '9780141439518',
      '9780684803357'
    ]
  },
  {
    firstName: 'Alexander',
    lastName: 'Roberts',
    address: {
      street: '3535 Aspen Lane',
      city: 'Phoenix',
      state: 'AZ',
      zip: '85001'
    },
    books: [ '9781501180989', '9780143035008' ]
  },
  {
    firstName: 'Madison',
    lastName: 'Carter',
    address: {
      street: '3636 Walnut Drive',
      city: 'Portland',
      state: 'OR',
      zip: '97035'
    },
    books: [
      '9780142004234',
      '9780439023481',
      '9780316769488',
      '9780060883286',
      '9780060883286',
      '9780062797155',
      '9780439023481'
    ]
  },
  {
    firstName: 'Jacob',
    lastName: 'Phillips',
    address: {
      street: '3737 Willow Road',
      city: 'Boston',
      state: 'MA',
      zip: '02101'
    },
    books: [ '9781451626650' ]
  },
  {
    firstName: 'Sydney',
    lastName: 'Evans',
    address: {
      street: '3838 Maple Street',
      city: 'Austin',
      state: 'TX',
      zip: '73301'
    },
    books: []
  },
  {
    firstName: 'Samuel',
    lastName: 'Turner',
    address: {
      street: '3939 Oak Avenue',
      city: 'Los Angeles',
      state: 'CA',
      zip: '90001'
    },
    books: []
  },
  {
    firstName: 'Samuel',
    lastName: 'Turner',
    address: {
      street: '3939 Oak Avenue',
      city: 'Los Angeles',
      state: 'CA',
      zip: '90001'
    },
    books: [ '9780399501487', '9780451526342' ]
  }
];

const LIBRARY = [
  {
    title: 'To Kill a Mockingbird',
    ISBN: '9780061120084',
    author: 'Harper Lee',
    year: 1960
  },
  {
    title: '1984',
    ISBN: '9780451524935',
    author: 'George Orwell',
    year: 1949
  },
  {
    title: 'The Great Gatsby',
    ISBN: '9780743273565',
    author: 'F. Scott Fitzgerald',
    year: 1925
  },
  {
    title: 'The Catcher in the Rye',
    ISBN: '9780316769488',
    author: 'J.D. Salinger',
    year: 1951
  },
  {
    title: 'The Hobbit',
    ISBN: '9780547928227',
    author: 'J.R.R. Tolkien',
    year: 1937
  },
  {
    title: 'Fahrenheit 451',
    ISBN: '9781451673319',
    author: 'Ray Bradbury',
    year: 1953
  },
  {
    title: 'Jane Eyre',
    ISBN: '9780141441146',
    author: 'Charlotte Bronte',
    year: 1847
  },
  {
    title: 'Pride and Prejudice',
    ISBN: '9780141439518',
    author: 'Jane Austen',
    year: 1813
  },
  {
    title: 'Animal Farm',
    ISBN: '9780451526342',
    author: 'George Orwell',
    year: 1945
  },
  {
    title: 'The Lord of the Rings',
    ISBN: '9780544003415',
    author: 'J.R.R. Tolkien',
    year: 1954
  },
  {
    title: 'The Catch-22',
    ISBN: '9781451626650',
    author: 'Joseph Heller',
    year: 1961
  },
  {
    title: 'The Chronicles of Narnia',
    ISBN: '9780066238500',
    author: 'C.S. Lewis',
    year: 1950
  },
  {
    title: 'Moby Dick',
    ISBN: '9781503280786',
    author: 'Herman Melville',
    year: 1851
  },
  {
    title: 'War and Peace',
    ISBN: '9781400079988',
    author: 'Leo Tolstoy',
    year: 1869
  },
  {
    title: 'Crime and Punishment',
    ISBN: '9780143058144',
    author: 'Fyodor Dostoevsky',
    year: 1866
  },
  {
    title: 'The Brothers Karamazov',
    ISBN: '9780374528379',
    author: 'Fyodor Dostoevsky',
    year: 1880
  },
  {
    title: 'Brave New World',
    ISBN: '9780060850524',
    author: 'Aldous Huxley',
    year: 1932
  },
  {
    title: 'Wuthering Heights',
    ISBN: '9780141439556',
    author: 'Emily Bronte',
    year: 1847
  },
  {
    title: 'Great Expectations',
    ISBN: '9780141439563',
    author: 'Charles Dickens',
    year: 1861
  },
  {
    title: 'A Tale of Two Cities',
    ISBN: '9780141439600',
    author: 'Charles Dickens',
    year: 1859
  },
  {
    title: 'The Adventures of Huckleberry Finn',
    ISBN: '9780486280615',
    author: 'Mark Twain',
    year: 1884
  },
  {
    title: 'Don Quixote',
    ISBN: '9780060934346',
    author: 'Miguel de Cervantes',
    year: 1605
  },
  {
    title: 'One Hundred Years of Solitude',
    ISBN: '9780060883286',
    author: 'Gabriel Garcia Marquez',
    year: 1967
  },
  {
    title: 'Les Misérables',
    ISBN: '9780451419439',
    author: 'Victor Hugo',
    year: 1862
  },
  {
    title: 'The Odyssey',
    ISBN: '9780140268867',
    author: 'Homer',
    year: -800
  },
  {
    title: 'The Iliad',
    ISBN: '9780140275360',
    author: 'Homer',
    year: -750
  },
  {
    title: 'Madame Bovary',
    ISBN: '9780140449129',
    author: 'Gustave Flaubert',
    year: 1856
  },
  {
    title: 'The Divine Comedy',
    ISBN: '9780142437223',
    author: 'Dante Alighieri',
    year: 1320
  },
  {
    title: 'The Count of Monte Cristo',
    ISBN: '9780140449266',
    author: 'Alexandre Dumas',
    year: 1844
  },
  {
    title: 'Dracula',
    ISBN: '9780141439846',
    author: 'Bram Stoker',
    year: 1897
  },
  {
    title: 'Frankenstein',
    ISBN: '9780141439471',
    author: 'Mary Shelley',
    year: 1818
  },
  {
    title: 'The Picture of Dorian Gray',
    ISBN: '9780141439570',
    author: 'Oscar Wilde',
    year: 1890
  },
  {
    title: 'Anna Karenina',
    ISBN: '9780143035008',
    author: 'Leo Tolstoy',
    year: 1877
  },
  {
    title: 'Ulysses',
    ISBN: '9780199535675',
    author: 'James Joyce',
    year: 1922
  },
  {
    title: 'Lolita',
    ISBN: '9780679723165',
    author: 'Vladimir Nabokov',
    year: 1955
  },
  {
    title: 'The Sound and the Fury',
    ISBN: '9780679732242',
    author: 'William Faulkner',
    year: 1929
  },
  {
    title: 'Slaughterhouse-Five',
    ISBN: '9780440180296',
    author: 'Kurt Vonnegut',
    year: 1969
  },
  {
    title: 'Gone with the Wind',
    ISBN: '9781451635621',
    author: 'Margaret Mitchell',
    year: 1936
  },
  {
    title: 'The Grapes of Wrath',
    ISBN: '9780143039433',
    author: 'John Steinbeck',
    year: 1939
  },
  {
    title: 'East of Eden',
    ISBN: '9780142004234',
    author: 'John Steinbeck',
    year: 1952
  },
  {
    title: 'The Sun Also Rises',
    ISBN: '9780743297332',
    author: 'Ernest Hemingway',
    year: 1926
  },
  {
    title: 'For Whom the Bell Tolls',
    ISBN: '9780684803357',
    author: 'Ernest Hemingway',
    year: 1940
  },
  {
    title: 'Catch-22',
    ISBN: '9781451626650',
    author: 'Joseph Heller',
    year: 1961
  },
  {
    title: 'The Old Man and the Sea',
    ISBN: '9780684801223',
    author: 'Ernest Hemingway',
    year: 1952
  },
  {
    title: 'Moby Dick',
    ISBN: '9781503280786',
    author: 'Herman Melville',
    year: 1851
  },
  {
    title: 'The Scarlet Letter',
    ISBN: '9780142437261',
    author: 'Nathaniel Hawthorne',
    year: 1850
  },
  {
    title: 'The Metamorphosis',
    ISBN: '9780553213690',
    author: 'Franz Kafka',
    year: 1915
  },
  {
    title: 'Heart of Darkness',
    ISBN: '9780141441672',
    author: 'Joseph Conrad',
    year: 1899
  },
  {
    title: 'The Trial',
    ISBN: '9780805209990',
    author: 'Franz Kafka',
    year: 1925
  },
  {
    title: 'The Stranger',
    ISBN: '9780679720201',
    author: 'Albert Camus',
    year: 1942
  },
  {
    title: 'Beloved',
    ISBN: '9781400033416',
    author: 'Toni Morrison',
    year: 1987
  },
  {
    title: 'Invisible Man',
    ISBN: '9780679732761',
    author: 'Ralph Ellison',
    year: 1952
  },
  {
    title: 'The Catcher in the Rye',
    ISBN: '9780316769488',
    author: 'J.D. Salinger',
    year: 1951
  },
  {
    title: 'To Kill a Mockingbird',
    ISBN: '9780061120084',
    author: 'Harper Lee',
    year: 1960
  },
  {
    title: 'One Hundred Years of Solitude',
    ISBN: '9780060883286',
    author: 'Gabriel Garcia Marquez',
    year: 1967
  },
  {
    title: 'East of Eden',
    ISBN: '9780142004234',
    author: 'John Steinbeck',
    year: 1952
  },
  {
    title: 'The Old Man and the Sea',
    ISBN: '9780684801223',
    author: 'Ernest Hemingway',
    year: 1952
  },
  {
    title: 'Lord of the Flies',
    ISBN: '9780399501487',
    author: 'William Golding',
    year: 1954
  },
  {
    title: 'Of Mice and Men',
    ISBN: '9780140177398',
    author: 'John Steinbeck',
    year: 1937
  },
  {
    title: 'A Clockwork Orange',
    ISBN: '9780393312836',
    author: 'Anthony Burgess',
    year: 1962
  },
  {
    title: 'The Road',
    ISBN: '9780307387899',
    author: 'Cormac McCarthy',
    year: 2006
  },
  {
    title: 'The Book Thief',
    ISBN: '9780375842206',
    author: 'Markus Zusak',
    year: 2005
  },
  {
    title: 'The Kite Runner',
    ISBN: '9781594631931',
    author: 'Khaled Hosseini',
    year: 2003
  },
  {
    title: 'Life of Pi',
    ISBN: '9780156027328',
    author: 'Yann Martel',
    year: 2001
  },
  {
    title: 'Memoirs of a Geisha',
    ISBN: '9780679781585',
    author: 'Arthur Golden',
    year: 1997
  },
  {
    title: 'The Girl with the Dragon Tattoo',
    ISBN: '9780307454546',
    author: 'Stieg Larsson',
    year: 2005
  },
  {
    title: 'The Help',
    ISBN: '9780399155345',
    author: 'Kathryn Stockett',
    year: 2009
  },
  {
    title: 'The Da Vinci Code',
    ISBN: '9780307474278',
    author: 'Dan Brown',
    year: 2003
  },
  {
    title: 'The Alchemist',
    ISBN: '9780061122415',
    author: 'Paulo Coelho',
    year: 1988
  },
  {
    title: 'The Catch-22',
    ISBN: '9781451626650',
    author: 'Joseph Heller',
    year: 1961
  },
  {
    title: 'The Hunger Games',
    ISBN: '9780439023481',
    author: 'Suzanne Collins',
    year: 2008
  },
  {
    title: 'Dune',
    ISBN: '9780441172719',
    author: 'Frank Herbert',
    year: 1965
  },
  {
    title: 'The Shining',
    ISBN: '9780307743657',
    author: 'Stephen King',
    year: 1977
  },
  {
    title: "Harry Potter and the Sorcerer's Stone",
    ISBN: '9780590353427',
    author: 'J.K. Rowling',
    year: 1997
  },
  {
    title: 'Harry Potter and the Chamber of Secrets',
    ISBN: '9780439064873',
    author: 'J.K. Rowling',
    year: 1998
  },
  {
    title: 'Harry Potter and the Prisoner of Azkaban',
    ISBN: '9780439136365',
    author: 'J.K. Rowling',
    year: 1999
  },
  {
    title: 'Harry Potter and the Goblet of Fire',
    ISBN: '9780439139601',
    author: 'J.K. Rowling',
    year: 2000
  },
  {
    title: 'Harry Potter and the Order of the Phoenix',
    ISBN: '9780439358071',
    author: 'J.K. Rowling',
    year: 2003
  },
  {
    title: 'Harry Potter and the Half-Blood Prince',
    ISBN: '9780439785969',
    author: 'J.K. Rowling',
    year: 2005
  },
  {
    title: 'Harry Potter and the Deathly Hallows',
    ISBN: '9780545010221',
    author: 'J.K. Rowling',
    year: 2007
  },
  {
    title: 'The Maze Runner',
    ISBN: '9780385737951',
    author: 'James Dashner',
    year: 2009
  },
  {
    title: 'Divergent',
    ISBN: '9780062024039',
    author: 'Veronica Roth',
    year: 2011
  },
  {
    title: 'The Fault in Our Stars',
    ISBN: '9780525478812',
    author: 'John Green',
    year: 2012
  },
  {
    title: 'Gone Girl',
    ISBN: '9780307588371',
    author: 'Gillian Flynn',
    year: 2012
  },
  {
    title: 'The Girl on the Train',
    ISBN: '9781594634024',
    author: 'Paula Hawkins',
    year: 2015
  },
  {
    title: "The Handmaid's Tale",
    ISBN: '9780385490818',
    author: 'Margaret Atwood',
    year: 1985
  },
  {
    title: 'The Road',
    ISBN: '9780307387899',
    author: 'Cormac McCarthy',
    year: 2006
  },
  {
    title: 'The Light We Lost',
    ISBN: '9780735212763',
    author: 'Jill Santopolo',
    year: 2017
  },
  {
    title: 'Little Fires Everywhere',
    ISBN: '9780735224292',
    author: 'Celeste Ng',
    year: 2017
  },
  {
    title: 'Where the Crawdads Sing',
    ISBN: '9780735219090',
    author: 'Delia Owens',
    year: 2018
  },
  {
    title: 'Educated',
    ISBN: '9780399590504',
    author: 'Tara Westover',
    year: 2018
  },
  {
    title: 'Becoming',
    ISBN: '9781524763138',
    author: 'Michelle Obama',
    year: 2018
  },
  {
    title: 'The Nightingale',
    ISBN: '9781250080400',
    author: 'Kristin Hannah',
    year: 2015
  },
  {
    title: 'The Tattooist of Auschwitz',
    ISBN: '9780062797155',
    author: 'Heather Morris',
    year: 2018
  },
  {
    title: 'Circe',
    ISBN: '9780316556347',
    author: 'Madeline Miller',
    year: 2018
  },
  {
    title: 'The Song of Achilles',
    ISBN: '9780062060627',
    author: 'Madeline Miller',
    year: 2011
  },
  {
    title: 'The Goldfinch',
    ISBN: '9780316055437',
    author: 'Donna Tartt',
    year: 2013
  },
  {
    title: 'The Secret History',
    ISBN: '9781400031702',
    author: 'Donna Tartt',
    year: 1992
  },
  {
    title: 'The Silent Patient',
    ISBN: '9781250301697',
    author: 'Alex Michaelides',
    year: 2019
  },
  {
    title: 'Normal People',
    ISBN: '9781984822178',
    author: 'Sally Rooney',
    year: 2018
  }
];

export { LIBRARY, STUDENTS };
