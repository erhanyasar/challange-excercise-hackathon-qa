// fetches data
import { LIBRARY, STUDENTS } from "./constants.js";
 
function main() {    
    // TODO-1: Dedupe students and library
    const [students, library] = deDuplicate(STUDENTS, LIBRARY);
    console.log("Students lengths should change from 51 to 43; ", STUDENTS.length, students.length);
    console.log("Library length should change from 100 to 91; ", LIBRARY.length, library.length);
    
    // TODO-2: Resolve all books for every student
    const resolvedStudents = resolveBooks(students, library);
    console.log(
        `The book object logged below should be same as {"title":"Where the Crawdads Sing","ISBN":"9780735219090","author":"Delia Owens","year":2018};\n`,
        resolvedStudents[0]?.books?.[0]
    );
    
    // TODO-3: Get the top 5 most read students
    const top5 = top5Students(resolvedStudents);
    console.log(
        `The top5 object logged below should be same as {"Alexandra King":9,"Joshua Garcia":8,"Hannah Taylor":8,"Justin Baker":8,"Anthony Campbell":8};\n`,
        top5
    )
}

main();

/** 
 * Function deDuplicate() should return a tuple of students and 
 * library where duplicate entries have been removed.
 * 
 * @param {Student[]} students 
 * @param {Book[]} library
 * @returns {[Student[], Book[]]}
 */

function deDuplicate(students, library) {
    function removeDuplicates(arr) {
        let resultArray=[];

        /** Brute force solution with `for loops` at O(n)2 time complexity
        for (let i=0; i< arr.length; i++) {
            let found=false;

            for (let j=i+1; j<arr.length; j++) {
                if (
                    Object.hasOwn(arr[i], "firstName") && arr[i].firstName === arr[j].firstName && arr[i].lastName === arr[j].lastName
                    )
                    found=true;
                else if (
                    Object.hasOwn(arr[i], "ISBN") && arr[i].ISBN === arr[j].ISBN
                )
                    found=true;
            }
            if (!found) resultArray.push(arr[i]);
        }
        */

        // Refactoring with Array.filter at O(n) time complexity
        let uniquesArray;

        if (Object.hasOwn(arr[0], "firstName")) {
            uniquesArray = students.map(({firstName, lastName}) => `${firstName} ${lastName}`);
            resultArray = students.filter(({firstName, lastName}, index) => !uniquesArray.includes(`${firstName} ${lastName}`, index+1));
        } else if (Object.hasOwn(arr[0], "ISBN")) {
            uniquesArray = library.map(({ISBN}) => ISBN);
            resultArray = library.filter(({ISBN}, index) => !uniquesArray.includes(`${ISBN}`, index+1));
        }

        return resultArray;
    }

    return [removeDuplicates(students), removeDuplicates(library)];
}

/** 
 * Function resolveBooks() should return a Student[] where books have been resolved.
 * 
 * @param {Student[]} students 
 * @param {Book[]} library
 * @returns {(Omit<Student, 'books'> & { books: Book[] })[]}
 */

function resolveBooks(students, library) {
    // Notice `students.books` array objects are frozen
    let studentsWithResolvedBooks = JSON.parse(JSON.stringify(students));
    
    for (let i=0; i < students.length; i++) {
        for (let j=0; j < students[i].books?.length; j++) {
            let resolved = library.find(book => book.ISBN === students[i].books[j]);

            if (resolved) studentsWithResolvedBooks[i].books.splice(j, 1, resolved);
        }
    }

    return studentsWithResolvedBooks;
}

/**
 * @param {ReturnType<typeof resolveBooks>} students
 * @returns {Record<string, number>};
 */

function top5Students(students) {
    let counts=[],
        sortedStudents=[],
        top5={};

    students.forEach(student => {
        let counter=0;

        for (let i=0; i<student.books.length; i++) counter++;
        
        counts.push({counter, name: `${student.firstName} ${student.lastName}`});
    });

    sortedStudents=counts.sort((a, b) => a.counter-b.counter).reverse();

    for (let i=0; i<5; i++)
        top5 = {
            ...top5,
            [sortedStudents[i].name]: sortedStudents[i].counter
        }

    return top5;
}