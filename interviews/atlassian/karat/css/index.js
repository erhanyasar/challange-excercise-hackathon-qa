String.prototype.repeatify = function (num) {
    let result="";

    for (let i=0; i<num; i++) {
        result += this + ', ';
    }

    return result.slice(0, -2);
}

console.log('hello'.repeatify(3));
