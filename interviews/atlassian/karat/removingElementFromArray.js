const animals = ["dog", "cat", "mouse", "cat"];

const removeTargetElement = (arr, target) => {
    let newArr = arr.slice();

    newArr.splice(newArr.indexOf(target), 1);

    return newArr;
}


console.log(removeTargetElement(animals, "cat"));