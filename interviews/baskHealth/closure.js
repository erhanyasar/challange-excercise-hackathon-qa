function createCounter() {
    let count = 0;

    return function() {
        count++;
        console.log(count);
    }
}

const counter1 = createCounter(), // New closure with count = 0
    counter2 = createCounter(); // Another closure with count = 0


counter1(); // 1
counter1(); // 2
counter2(); // 1