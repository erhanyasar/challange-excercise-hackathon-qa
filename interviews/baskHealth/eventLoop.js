console.log('Start');

setTimeout(() => console.log('Timeout'), 0);

Promise.resolve().then(() => console.log('Promise 1'));

async function asyncFunction(params) {
    console.log('Async Function Start');
    await Promise.resolve();
    console.log('Async Function End');
}

asyncFunction();

console.log('End');

/**
 * Start
 * Async Function Start
 * End
 * Promise 1
 * Async Function End
 * Timeout
 */