const promise1 = Promise.resolve('1');
const promise2 = 2;
const promise3 = Promise.reject("3");
const promise4 = new Promise((resolve, reject) => setTimeout(resolve, 100, 2 + 2 ));

Promise.all([
    promise1,
    promise2,
    promise3,
    promise4
]).then((values) => console.log(values));

/**
 * What's the output of the code above?
 * 
 * A. [1, 2, 3, 4]
 * B. ['1', 2, "3", 2]
 * C. [1, 2, 3, 4]
 * D. Error
 */