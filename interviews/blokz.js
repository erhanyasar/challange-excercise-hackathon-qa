/**
 * Write a function that accepts a number and returns the calculated depth for given number.
 * 
 * @param {number} x
 * @returns number
 */

function computeDepth(x = 42) {
  let arrayOfDigits = new Set(),
    index = 0;

  while (Array.from(arrayOfDigits).length < 10) {
    let arrayOfDigitsArr = (x * (index + 1)).toString().split("");
    arrayOfDigitsArr.forEach((element) => arrayOfDigits.add(element));
    index++;
  }

  return index;
}

console.log(computeDepth());