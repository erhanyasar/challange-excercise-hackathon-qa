/**
 * Complete the `blocker` function below. It should only be written to the lines between two `console.log`s
 * below in a way that the second `console.log` to be blocked for 1 second without touching any other lines.
 */

const blocker = async () => {
    console.log(1);
    /* Write your code only on this line here */ await new Promise(resolve => setTimeout(() => resolve(), 1000));
    console.log(2);
}

blocker();