/**
 * Write a curry function that accepts either one or two parameters but not affecting the output
 * either way chosen. It should return boolean deciding whether the first parameter is less than
 * the latter or not.
 * 
 * @param {number} a 
 * @param {number} b 
 * @returns boolean
 */

const curry = (a, b) => !b ? b => a < b : a < b;

console.log(curry(2)(4), curry(2, 4));