/**
 * Write a function that finds the count of occurences for a given word as a
 * seperate letters inside given string
 * 
 * @param {string} word
 * @param {string} pool
 * @returns number
 */

const findOccurencesOfWord = (word = "word", pool = "dworsdfwsodrwword") => {
  let obj = {};

  for (let i = 0; i < word.length; i++) obj[word[i]] = 0;

  for (let i = 0; i < pool.length; i++) {
    for (let j = 0; j < word.length; j++) {
      if (pool[i] === word[j]) obj[word[j]] += 1;
    }
  }

  return Object.values(obj).sort((a, b) => a - b)[0];
};

console.log(findOccurencesOfWord());