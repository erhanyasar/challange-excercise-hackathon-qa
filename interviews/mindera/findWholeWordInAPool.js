/**
 * Write a function that finds the count of occurences for a given word as a whole inside given string
 * 
 * @param {string} word
 * @param {string} pool
 * @returns number
 */

const findWholeWordInAPool = (word = "word", pool = "dworsdfwsodrwword") => {
  let regexp = new RegExp(word);
  return (pool.match(regexp) || []).length;
};

console.log(findWholeWordInAPool());