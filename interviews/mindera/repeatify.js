/**
 * Write a built-in string function called `repeatify`. It accepts a number as a parameter
 * and returns a string that's repeating the original string for a given number param.
 * 
 * For instance, `"hello".repeatify(3)` expression should return "hello, hello, hello".
 * 
 * @param {number} param
 * @returns string
 */

String.prototype.repeatify = function (param) {
  let result = "";

  for (let i = 0; i < param; i++) result += `, ${this}`;

  return result.slice(1).trim();
};

console.log("hello".repeatify(3)); // "hello, hello, hello"